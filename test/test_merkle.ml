open Printf
open Merkle

let error_occurred = ref false

let function_tested = ref ""

let testing_function s =
  function_tested := s;
  print_newline();
  print_string s;
  print_newline()

let test expect actual =
  if expect <> actual then begin
    eprintf "*** Bad result (%s)\n" !function_tested;
    flush stderr;
    error_occurred := true
  end
  
let _ =
  testing_function "is_same";
  let mtree = create Hash.debug "1"
  in
    add "2" mtree;  
    add "3" mtree;  
    test true (is_same (create_all Hash.debug ("1" :: "2" :: "3" :: [])) mtree)

let _ = 
  testing_function "root";
  let mtree = create Hash.debug "1"
in
  test true (root "h1" mtree)

let _ = 
  testing_function "member";
  let mtree = create_all Hash.debug ("1" :: "2" :: "3" :: [])
in
  test true (member "h3" mtree);
  test true (member "hh1h3" mtree)

let _ =
  testing_function "create_many";
  let mtree = create_many Hash.debug 100
in
  test false (root "h10" mtree)

let _ =
  print_newline();
  if !error_occurred then begin
    printf "********* TEST FAILED ***********\n";
    exit 2 
  end else begin
    printf "All tests successful.\n";
    exit 0
  end