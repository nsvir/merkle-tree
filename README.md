# Merkle tree

Create and print a merkle tree that grows from the leaves:
![algorithm](./Merkle_tree.png)

# [Documentation](./lib/merkle.mli)

# Run demo

dune exec merkle

# Run test

dune runtest

# Run utop with library

Install cryptohash library
```bash
opam install cryptokit
```

```bash
dune utop
let mtree1 = Merkle.create_many Merkle.Hash.sha512 10;;
let mtree2 = Merkle.create_all Merkle.Hash.sha512 ("1" :: "2" :: "3" :: []);;
let mtree3 = Merkle.create Merkle.Hash.sha512 "0";;
Merkle.add "1" mtree3;;
Merkle.add "2" mtree3;;
Merkle.is_same mtree2 mtree3;;
Merkle.print mtree;;
```
