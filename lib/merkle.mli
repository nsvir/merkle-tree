exception Unexpected

type merkle_tree

module Hash :
  sig
    val sha512 : string -> string
    val debug : string -> string
  end

(* create a merkle_tree from a single value *)
val create : (string -> string) -> string -> merkle_tree

(* create a merkle_tree from a list of values *)
val create_all : (string -> string) -> string list -> merkle_tree

(* create merkle_tree with random elements *)
val create_many : (string -> string) -> int -> merkle_tree

(* add a leaf to the merkle_tree *)
val add : string -> merkle_tree -> unit

(* deep compare two merkle_trees *)
val is_same : merkle_tree -> merkle_tree -> bool

(* check the hash value of the root *)
val root : string -> merkle_tree -> bool

(* check the hash is found in the merkle_tree *)
val member : string -> merkle_tree -> bool

(* print small merkle_tree *)
val print : merkle_tree -> unit
