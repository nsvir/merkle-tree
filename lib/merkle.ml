exception Unexpected

type tree =
  | Empty
  | Leaf of leaf
  | Node of node

and leaf = {
  data: string;
  lhash: string;
  mutable lparent: tree;
}

and node = {
  mutable left: tree;
  mutable nhash: string;
  mutable nparent: tree;
  mutable right: tree
}

type merkle_tree = {
  mutable root: tree;
  hash: (string -> string)
}

module Tree = struct

  let create_leaf value hash = {
    lhash = hash value;
    data = value;
    lparent = Empty
  }

  let is_leaf = function
  | Leaf _ -> true
  | Empty | Node _ -> false

  (* find the first top left leaf *)
  let first_leaf tree: leaf =
    let rec aux acc =
      match Queue.take acc with
      | Empty -> raise Unexpected
      | Leaf leaf -> leaf
      | Node {left; right; _} ->
          Queue.add left acc;
          Queue.add right acc;
          aux acc
    and acc = Queue.create ()
    in 
      Queue.add tree acc;
      aux acc

  let insert_sibling leaf new_leaf =
    let new_node = {
      left = Leaf leaf;
      nhash = "";
      right = Leaf new_leaf;
      nparent = leaf.lparent
    }
    in
      new_leaf.lparent <- Node new_node;
      begin match leaf.lparent with
      | Empty | Leaf _ -> ()
      | Node ({left; _} as parent_node) ->
        if is_leaf left then
          parent_node.left <- Node new_node
        else
          parent_node.right <- Node new_node;
      end;
      leaf.lparent <- Node new_node;
      Node new_node

  let is_same_leaf leaf1 leaf2 = leaf1.lhash = leaf2.lhash
  let is_same_node node1 node2 = node1.nhash = node2.nhash
end

module Hash = struct

  let sha512 x = (Cryptokit.hash_string (Cryptokit.Hash.sha2 512)) x
  let debug x = "h"^x

  let hash_from = function
    | Empty -> raise Unexpected
    | Leaf {lhash; _} -> lhash
    | Node {nhash; _} -> nhash

  let rec update_hash_to_root hash = function
    | Empty -> raise Unexpected
    | Leaf { lparent = Empty; _} as root -> root
    | Node ({nparent = Empty; _} as node) as root -> 
      node.nhash <- hash((hash_from node.left) ^ (hash_from node.right));
      root
    | Leaf {lparent; _} -> update_hash_to_root hash lparent
    | Node ({nparent; _} as node)-> 
      node.nhash <- hash((hash_from node.left) ^ (hash_from node.right));
      update_hash_to_root hash nparent
end

module Print = struct
  let to_val (tree: tree): ((int * string)  List.t) =
    let rec aux (values: (int * string) Queue.t) (acc: (tree * int) Queue.t): unit = 
      try match (Queue.take acc) with
        | (Empty, _) -> raise Unexpected
        | (Leaf {lhash; _}, depth) ->
          Queue.add (depth, lhash) values;
          aux values acc
        | (Node {left; nhash; right; _}, depth) ->
            Queue.add (depth, nhash) values;
            Queue.add (left, depth + 1) acc;
            Queue.add (right, depth + 1) acc;
            aux values acc
      with Queue.Empty -> ()
    and values = (Queue.create ())
    and acc = (Queue.create ())
    in
      Queue.add (tree, 0) acc;
      aux values acc;
      List.of_seq (Queue.to_seq values)


let print_tree tree_list =
  let rec aux curr_depth col = function
    | [] -> print_string "\n"
    | (depth, value) :: tail -> 
        if curr_depth != depth then begin
          Printf.printf "\n(%s)" value;
          aux depth 1 tail
        end else begin
          Printf.printf " (%s)" value;
          aux depth (col + 1) tail
        end
  in aux (-1) 0  tree_list;;

end

open Tree
open Hash
open Print

let add value mtree =
  let new_leaf = create_leaf value mtree.hash
  in let first_leaf = first_leaf mtree.root
  in let new_node = insert_sibling first_leaf new_leaf
  in let new_root = update_hash_to_root mtree.hash new_node
  in mtree.root <- new_root

let create hash value = {
  root = Leaf (create_leaf value hash);
  hash = hash
}

let create_all hash values = 
  let rec create_all_rec mtree = function
    | [] -> mtree
    | value :: tail -> 
      add value mtree;
      create_all_rec mtree tail
  in match values with
  | head :: tail -> create_all_rec (create hash head) tail
  | _ -> raise Unexpected

let create_many hash count =
  Random.self_init ();
  create_all hash (List.init count (fun _ -> string_of_int (Random.int 9)))

let is_same mtree1 mtree2 =
  let rec aux tree1 tree2 =
    match (tree1, tree2) with
    | (Leaf (_ as leaf1), Leaf (_ as leaf2)) ->
      is_same_leaf leaf1 leaf2
    | (Node (_ as node1), Node (_ as node2)) -> 
      is_same_node node1 node2
      && aux node1.left node2.left
      && aux node1.right node2.right
    | _ -> false
  in aux mtree1.root mtree2.root

let root hash mtree =
  match mtree.root with
  | Leaf {lhash; _} -> lhash = hash
  | Node {nhash; _} -> nhash = hash
  | _ -> false


let member hash mtree = 
  let rec member_rec = function
  | Leaf {lhash; _} -> lhash = hash
  | Node {nhash; left; right; _} -> nhash = hash || member_rec left || member_rec right
  | _ -> false
  in member_rec mtree.root

let print mtree = print_tree (to_val mtree.root)