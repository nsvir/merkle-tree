
let mtree = Merkle.create Merkle.Hash.debug "0"
in
  Merkle.add "1" mtree;
  Merkle.add "2" mtree;
  Merkle.add "3" mtree;
  Merkle.add "4" mtree;
  Merkle.add "5" mtree;
  Merkle.add "6" mtree;
  Merkle.add "7" mtree;
  Merkle.print mtree